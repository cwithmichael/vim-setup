#!/bin/bash
if command -v pacman &> /dev/null
then
    echo "OS: Arch"
    sudo pacman -Syu && sudo pacman -S vim cmake git htop go
fi 
if command -v apt-get &> /dev/null
then 
    echo "OS: Debian/Ubuntu"
    sudo apt-get update && sudo apt-get install build-essential cmake vim python3-dev python3-pip git vim
fi
echo "filetype plugin indent on
syntax on
set nu!
set autochdir
set ts=4
set bs=2
set expandtab
set encoding=utf-8
au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2" >> ~/.vimrc

git clone https://github.com/fatih/vim-go.git ~/.vim/pack/plugins/start/vim-go
git clone https://github.com/ycm-core/YouCompleteMe.git ~/.vim/pack/plugins/start/ycm

cd ~/.vim/pack/plugins/start/ycm/ && git submodule update --init --recursive && python3 install.py --clangd-completer --go-completer
